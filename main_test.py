###############################################################################
# (c) Copyright 2012-2016 CERN                                                #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# License version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this license, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''

Simple sample python class for testing in jenkins

Created on November 07, 2016
@author: Stefan-Gabriel Chitic
'''
import unittest
import main

class MyFirstTest(unittest.TestCase):


    def test_Object_instantiation(self):
        """
        Test if the object is assigned correct. The variable inside the object
        should be 0 and not -1
        """
        obj = main.MyFisrtClass()
        self.assertEqual(obj.getVariable(),0)

    def test_sumVariable(self):
        """
        Test if the sum function of the object is executed correctly
        """
        obj = main.MyFisrtClass()
        self.assertEqual(obj.sumVariable(10), 55)

    def test_printVersion(self):
        """
        Print the python version in use
        """
        print(main.checkPythonVersion())


