###############################################################################
# (c) Copyright 2012-2016 CERN                                                #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# License version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this license, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''

Simple python class to be used in the testing sample

Created on November 07, 2016
@author: Stefan-Gabriel Chitic
'''
import sys

class MyFisrtClass():
    """
    First sample class to be used for testing
    """

    myVariable = -1

    def __init__(self):
        """
        At init, the variable is assigned to 0
        """
        self.myVariable = 0

    def sumVariable(self,n):
        """
        Simple sum to test if the function is called correctly
        """
        s = 0
        for i in range(0,n+1):
             s += i
        return s

    def assignVariable(self,n):
        """
        This function is not tested in order to be reported in the coverage.py
        """
        self.myVariable = self.sumVariable(n)

    def getVariable(self):
        """
        Return the class variable
        """
        return self.myVariable

def checkPythonVersion():
    """
    Prints the python version
    """
    print(sys.version)
