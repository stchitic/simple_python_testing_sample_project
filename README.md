Simple python test sample
=========

[![Build Status](https://jenkins-lhcb-core-soft.web.cern.ch/job/Samples/job/Python/job/Sample%20Multiple%20Python%20version%20testing%20project/badge/icon)](https://jenkins-lhcb-core-soft.web.cern.ch/job/Samples/job/Python/job/Sample%20Multiple%20Python%20version%20testing%20project/)

main.py
--------

- a simple class that provides a setter and a getter for a local variable as well as a loop to sum up number
- a python version detection function


main_test.py
--------------------------

- an example class to test main.py

